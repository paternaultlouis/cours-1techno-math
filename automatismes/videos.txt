# Proportions

- Calculer une proportion, appliquer une porportion
  https://www.youtube.com/watch?v=r8S46rk9x9k

- Calculer la proportion d'une proportion
  https://www.youtube.com/watch?v=nPPRsOW2veU
  
# Images et Antécédents

- Calculer l'image d'un nombre
  https://www.youtube.com/watch?v=8j_4DHWnRJU
  
- Déterminer un antécédent par le calcul
  https://www.youtube.com/watch?v=X0oOBo65YpE
  
- Lire graphiquement une image et un antécédent
  https://www.youtube.com/watch?v=8cytzglu8yc
  
- Résoudre une équation par lecture graphiquement
  https://www.youtube.com/watch?v=FCUd2muFEyI

# Équations & Inéquations

- Résoudre une équation
  https://www.youtube.com/watch?v=6gW4rXWr3fY

- Résoudre une inéquation 1
  https://www.youtube.com/watch?v=ycYfb8aHssY

- Résoudre une inéquation 2
  https://www.youtube.com/watch?v=p93oVqzvog8

# Résolution graphique

- Résoudre une inéquation
  https://www.youtube.com/watch?v=3_6LcpumUh4
  
- Tableau de variations
  https://www.youtube.com/watch?v=yGqqoBMq8Fw
  
- Tableau de signes
  https://www.youtube.com/watch?v=AZvjA44WfPw

# Développer et Factoriser

- Développer avec la double distributivité
  https://www.youtube.com/watch?v=YS-3JI_z2f0

- Factoriser avec un facteur commun
  https://www.youtube.com/watch?v=5dCsR85qd3k

- Développer avec une identité remarquable
  https://www.youtube.com/watch?v=U98Tk89SJ5M

- Factoriser avec une identité remarquable
  https://www.youtube.com/watch?v=T9T4IeYGEe4

# Droites

- Déterminer l'équation réduite à partir des coordonnées de deux points
  https://www.youtube.com/watch?v=tfagLy6QRuw

- Déterminer graphiquement l'équation d'une droite
  https://www.youtube.com/watch?v=xGNi2cQ6xU8

- Tracer une droite à partir de son équation réduite
  https://www.youtube.com/watch?v=xGNi2cQ6xU8

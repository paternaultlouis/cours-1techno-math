%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2021 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compiler avec lualatex:
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2122-pablo}
\usepackage{2122-pablo-paternault}
\usepackage{2122-pablo-math}
\usepackage{2122-pablo-tikz}

\usepackage[
  a5paper,
  includehead,
  headsep=4mm,
  margin=6mm,
]{geometry}
\usepackage{2122-pablo-header}
\fancyhead[L]{\textsc{Ch. 3 --- Dérivation}}

\usepackage{multicol}
\usepackage{emoji}

\begin{document}

\setcounter{section}{1}
\section{Fonction dérivée}
\fancyhead[R]{\textsc{2 --- Fonction dérivée}}

\begin{definition*}
  Soit $f$ une fonction définie sur un intervalle $I$. On dit que $f$ est \emph{dérivable} sur $I$ si elle est dérivable en tout nombre réel $a$ de $I$.

  On définit alors la \emph{fonction dérivée de $f$}, notée $f'$, qui à tout nombre $x$ de $I$, associe $f'(x)$, le nombre dérivé de $f$ en $x$.
\end{definition*}

\begin{propriete*}[Dérivée des fonctions usuelles]
  Toutes les fonctions décrites ici sont définies et dérivables sur $\mathbb{R}$.
  \begin{center}
    \begin{tabular}{rcc}
      \toprule
      \multicolumn{2}{c}{Fonction} & Fonction dérivée \\
      \midrule
      Fonction constante & $f(x)=k$ & $f'(x)=0$ \\
      Fonction identité & $f(x)=x$ & $f'(x)=1$ \\
      Fonction carrée & $f(x)=x^2$ & $f'(x)=2x$ \\
      Fonction cube & $f(x)=x^3$ & $f'(x)=3x^2$ \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{propriete*}

\begin{exemple}
  On définit $f$ et $g$ par : $f(x)=x$ et $g(x)=x^3$. Calculer les nombres suivants :

  \begin{multicols}{3}
    \begin{enumerate}[(a)]
      \item $f(2)=$
      \item $f'(2)=$
      \item $g(4)=$
      \item $g'(4)=$
      \item $g(-1)=$
      \item $g'(-1)=$
    \end{enumerate}
  \end{multicols}
\end{exemple}

\begin{propriete*}[Opération sur les fonctions]~
  Soient $u$ et $v$ deux fonctions définie sur un intervalle $I$, et $k$ un nombre réel.
  \begin{itemize}
    \item La fonction définie par $f(x)=k\times u(x)$ est dérivable sur $I$, et pour tout nombre $x\in I$, on a : $f'(x)=k\times u'(x)$.
    \item La fonction définie par $f(x)=u(x)+v(x)$ est dérivable sur $I$, et pour tout nombre $x\in I$, on a : $f'(x)= u'(x)+v'(x)$.
  \end{itemize}
\end{propriete*}

\begin{exemple}
  Déterminer l'expression des dérivées des fonctions suivantes.
  \begin{multicols}{2}
    \begin{enumerate}
      \item $f(x)=5$
      \item $g(x)=4x$
      \item $h(x)=-2x^3$
      \item $k(x)=5x-1$
      \item $l(x)=4x^2-2x+1$
      \item $m(x)=2x^3+6x^2-4x+7$
    \end{enumerate}
  \end{multicols}
\end{exemple}

\begin{exercice*}~
  \begin{itemize}
    \item 54 à 57 p. 152 ; 62 à 67 p. 152
    \item 59 p. 152
    \item 27, 28 p. 149
  \end{itemize}
\end{exercice*}

\section{Dérivée et Variations}
\fancyhead[R]{\textsc{3 --- Dérivée et Variations}}

\begin{propriete*}
  Soit $f$ une fonction définie et dérivable sur un intervalle $I$. Alors :
  \begin{itemize}
    \item si la fonction dérivée $f'$ est strictement positive, alors la fonction $f$ est croissante ;
    \item si la fonction dérivée $f'$ est strictement négative, alors la fonction $f$ est décroissante.
  \end{itemize}
\end{propriete*}

\begin{exemple}
  Soit $f$ une fonction. On connait le tableau de signes de la dérivée, donné dans le tableau suivant. Compléter le tableau de variations de $f$.
  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=1,espcl=2]
      {$x$ /1,
        $f'$ /1,
        $f$/2
      }
      {$-\infty$, $-2$, $1$, $5$, $+\infty$}%
      \tkzTabLine{,+,z,-,z,+,z,-}
    \end{tikzpicture}
  \end{center}
\end{exemple}

\begin{exemple}[\emoji{heart}]
  On considère la fonction $f$ définie sur $\mathbb{R}$ par :
  \[f(x)=2x^2-6x+1\]
  \begin{enumerate}
    \item Déterminer l'expression de $f'$.
    \item Dresser le tableau de signes de $f'$.
    \item En déduire le tableau de variations de $f$.
    \item Quels sont les extremums de $f$ ?
  \end{enumerate}
\end{exemple}

\begin{exemple}[\emoji{heart}]
  On définit la fonction $g$ sur $\mathbb{R}$ par :
  \[g(x)=x^3+3x^2-9x+5\]
  \begin{enumerate}
    \item Montrer que pour tout $x\in\mathbb{R}$, on a : $g'(x)=3(x+3)(x-1)$.
    \item Dresser le tableau de signes de $g'$.
    \item En déduire le tableau de variations de $g$.
    \item Quels sont les extremums de $g$ ?
  \end{enumerate}
\end{exemple}

\begin{exercice*}~
  \begin{itemize}
    \item Exercices 80, 85 p. 153.
    \item Problèmes 93, 94 p. 154.
  \end{itemize}
\end{exercice*}

\end{document}

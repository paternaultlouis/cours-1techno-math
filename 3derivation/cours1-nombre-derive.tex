%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2021-2022 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compiler avec lualatex:
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2122-pablo}
\usepackage{2122-pablo-paternault}
\usepackage{2122-pablo-math}
\usepackage{2122-pablo-tikz}

\usepackage[
  a5paper,
  includehead,
  margin=6mm,
]{geometry}
\usepackage{2122-pablo-header}
\fancyhead[L]{\textsc{Ch. 3 --- Dérivation}}
\fancyhead[R]{\textsc{1 --- Nombre dérivé}}

\usepackage{multicol}

\begin{document}

\section{Tangente à une courbe}

\begin{definition*}~
  \begin{itemize}
    \item Une \emph{sécante} à une courbe $\mathcal{C}$ passant par le point $A$ est une droite passant par $A$, et coupant la courbe en un autre point $M$.
    \item Lorsque le point $M$ se rapproche de $A$, il arrive que la sécante $(AM)$ se rapproche d'une \enquote{position limite}. Cette droite \enquote{limite} est alors appelée \emph{tangente à la courbe $\mathcal{C}$ au point $A$}.
  \end{itemize}
\end{definition*}

\begin{exemple}
  \begin{multicols}{2}
    Voici la courbe représentative de la fonction $f:x\mapsto x^2+1$, ainsi que le point $A(2;5)$.

    \begin{enumerate}
      \item Tracer plusieurs sécantes passant par le point $A$.
      \item Tracer, d'une couleur différente, la tangente à la courbe de $f$ passant par $A$.
    \end{enumerate}

    \columnbreak

    \begin{center}
      \begin{tikzpicture}[scale=1,very thick]
        \begin{axis}[
            xscale=.8,
            yscale=.7,
            xscale=1,
            domain=0:5,
            ultra thick,
            axis lines=middle,
                    %minor x tick num=9,
                    %minor y tick num=4,
            enlarge y limits=true,
                    %xtick={0, 10, ..., 50},
            ymin=0,
	    xmin=0, xmax=3.5,
                    %ytick={0, 1, ..., 12},
                    %major grid style={black, thick},
            grid=both,
		        /pgf/number format/.cd, use comma,
          ]
          \addplot[
                    %restrict y to domain=-400:700,
            red, ultra thick,
            smooth,
    ]{(x-2)*(x-3)*x+1};
          \draw (axis cs:1, 3) node{$\times$} node[below left]{$A$};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}

\begin{exercice*}
  Exercices 17, 18 p. 148.
\end{exercice*}

\section{Nombre dérivé}

\begin{definition*}
  On reprend une fonction $f$ de courbe $\mathcal{C}$, un point $A$ (d'abscisse $a$), et un nombre strictement positif $h$. Le point $M$ de coordonnées $M(a+h; f(a+h))$ est un point de $\mathcal{C}$, et $(AM)$ est une sécante à $\mathcal{C}$.

  Alors le nombre réel $\frac{f(a+h)-f(a)}{h}$ est le coefficient directeur de $(AM)$, et le \emph{taux de variations} de la fonction $f$ entre $a$ et $a+h$.

    \begin{center}
      \begin{tikzpicture}[scale=1,very thick]
        \begin{axis}[
            xscale=1.3,
            yscale=.6,
            xscale=1,
            domain=0:5,
            ultra thick,
            axis lines=middle,
                    %minor x tick num=9,
                    %minor y tick num=4,
            enlarge y limits=true,
                    %xtick={0, 10, ..., 50},
            ymin=0,
	    xmin=0, xmax=3.5,
                    %ytick={0, 1, ..., 12},
                    %major grid style={black, thick},
            grid=both,
		        /pgf/number format/.cd, use comma,
          ]
          \addplot[
                    %restrict y to domain=-400:700,
            red, ultra thick,
            smooth,
    ]{(x-2)*(x-3)*x+1};
          \draw (axis cs:1, 3) node{$\times$} node[below left]{$A$};
        \end{axis}
      \end{tikzpicture}
    \end{center}
\end{definition*}

\begin{definition*}
  Si le taux de variations d'une fonction $f$ entre $a$ et $a+h$ tend vers un nombre $l$ lorsque $h$ tend vers 0, alors on dit que $f$ est dérivable en $a$. Ce nombre est appelé le \emph{nombre dérivé de $f$ en $a$}, et se note $f'(a)$. On écrit :
  \[
    f'(a)=\lim_{h\rightarrow0}\frac{f(a+h)-f(a)}{h}
  \]
\end{definition*}

\section{Équation de la tangente}

\begin{definition*}
  Soit $f$ une fonction dérivable en un nombre $a$. On appelle \emph{tangente à $f$ au point d'abscisse $a$} la droite $T$, passant par le point de coordonnée $(a; f(a))$, et de coefficient directeur $f'(a)$.
\end{definition*}

\begin{propriete*}
  Si $f$ est dérivable en $a$, l'équation réduite de la tangente à la courbe de $f$ en $a$ est :
  \[
    y=f'(a)(x-a)+f(a)
  \]
\end{propriete*}

\begin{exemple}
  \begin{multicols}{2}
    On considère la fonction $f$ définie par $f(x)=x^2-2x+1$, tracée ci-contre.

    On admet que pour tout $x\in\mathbb{R}$, on a : $f'(x)=2x-2$.

    \begin{enumerate}
      \item Calculer $f(3)$ et $f'(3)$.
      \item Déterminer l'équation de la tangente à la courbe de $f$ au points d'abscisse 3.
      \item Tracer cette tangente.
    \end{enumerate}

    \columnbreak

    \begin{center}
      \begin{tikzpicture}[scale=1,very thick]
        \begin{axis}[
            yscale=.9,
            xscale=.8,
            xscale=1,
            domain=0:4,
            ultra thick,
            axis lines=middle,
                    %minor x tick num=9,
                    %minor y tick num=4,
            enlarge y limits=true,
                    %xtick={0, 10, ..., 50},
            ytick={0, 1, ..., 9},
            ymin=0,
                    %major grid style={black, thick},
            grid=both,
          ]
          \addplot[
                    %restrict y to domain=-400:700,
            red, ultra thick,
            smooth,
          ]{x*x-2*x+1};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}


\end{document}

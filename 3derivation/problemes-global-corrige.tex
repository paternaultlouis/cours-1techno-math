% Compiler avec lualatex:
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2425-pablo}
\usepackage{2425-pablo-paternault}
\usepackage{2425-pablo-math}
\usepackage{2425-pablo-tikz}

\usepackage[
  a5paper,
  includehead,
  margin=6mm,
]{geometry}
\usepackage{2425-pablo-header}
\fancyhead[L]{\textsc{Dérivation --- Point de vue global}}
\fancyhead[R]{\textsc{Sujets d'EC --- Corrigé}}

\begin{document}

\begin{exercice}[D'après l'exercice 03 du sujet d'EC \no17]
  \begin{em}
    La glycémie est la concentration massique exprimée en gramme par litre \si{g.L^{-1}} de sucre dans le sang. Le diabète se caractérise par une hyperglycémie chronique, c'est-à-dire un excès de sucre dans le sang et donc une glycémie trop élevée.

    Une glycémie est normale lorsqu'elle est comprise entre \SI{0,7}{g.L^{-1}} et \SI{1,1}{g.L^{-1}} à jeun et lorsqu'elle est inférieure à \SI{1,4}{g.L^{-1}}, une heure et trente minutes après un repas.

    Lorsque l'on suspecte un diabète, on pratique un test de tolérance au glucose. Lorsqu'il est à jeun, le patient ingère \SI{75}{g} de glucose au temps $t = 0$ ($t$ est exprimé en heure).

    Pour tout réel $t$ de l'intervalle $[0 ; 3]$, la glycémie du patient, exprimée en \si{g.L^{-1}}, $t$ heures après l'ingestion, est modélisée par la fonction $f$ définie sur $[0 ;3]$ par :
    \[ f (t ) = 0, 3t^3 - 1, 8t^2 + 2, 7t + 0,8\]
  \end{em}

  \begin{enumerate}
    \item \emph{Que valait la glycémie du patient à jeun ?}

      Le patient est à jeun au début du test, c'est-à-dire à $t=0$. Sa glycémie est alors :
      \[f(0)=0,3\times0^3-1,8\times0^2+2,7\times0+0,8=\qty{0,8}{g.L^{-1}}.\]
    \item
      \begin{enumerate}
        \item \emph{On note $f'$ la fonction dérivée de la fonction $f$ . Montrer que pour tout réel $t$ appartenant à $[0 ; 3]$ :}
          \[f ' (t ) = 0, 9(t - 1)(t - 3)\]

          Commençons par dériver la fonction $f$ :

          \begin{align*}
            f'(t)&=0,3\times3t^2-1,8\times2t+2,7\times1+0\\
            &=0,9t^2-3,6t+2,7\\
          \end{align*}

          Il faut maintenant factoriser cette expression, pour obtenir la forme demandée. Mais vous ne savez pas encore faire cela. Faisons donc l'inverse, et développons l'expression donnée.

          \begin{align*}
            0,9(t-1)(t-3)
            &=0,9\left( t^2-1\times t-3\times t+3 \right)\\
            &=0,9\left( t^2-4t+3\right)\\
            &=0,9t^2-0,9\times4t+0,9\times3\\
            &=0,9t^2-3,6t+2,7\\
            &=f'(t)
          \end{align*}

        \item \emph{Étudier le signe de $f' (t )$ sur $[0 ; 3]$ et en déduire le tableau de variations de la fonction $f$ sur l'intervalle $[0 ; 3]$.}

          La dérivée $f'$ est un polynôme du second degré de la forme $a\left( x-x_1 \right)\left( x-x_2 \right)$, avec $a=0,9$, $x_1=1$, et $x_2=3$. Puisque $a>0$, sa courbe est une parabole avec le sommet \enquote{en bas}. L'allure de sa courbe est donc la suivante.

          \begin{center}
      \begin{tikzpicture}[scale=1,very thick]
        \begin{axis}[
                %yscale=1.6,
            xscale=.9,
            domain=-1:4,
            ultra thick,
            axis lines=middle,
                    %minor x tick num=9,
                    %minor y tick num=4,
            enlarge y limits=true,
                    %xtick={0, 10, ..., 50},
                    %ytick={-500, -400, ..., 700},
                    %major grid style={black, thick},
            grid=both,
          ]
          \addplot[
            red, ultra thick,
            smooth,
          ]{0.9*(x-1)*(x-3)};
          \node (texte) at (axis cs:2, 2) {$x_1$, $x_2$};
          \node (x1) at (axis cs:1, 0) {$\bullet$};
          \node (x2) at (axis cs:3, 0) {$\bullet$};
          \draw[-latex, opacity=.5] (texte) -- (x1);
          \draw[-latex, opacity=.5] (texte) -- (x2);
        \end{axis}
      \end{tikzpicture}
    \end{center}

      Donc le tableau de signes de $f'$ est le suivant.

  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=1,espcl=2]
      {$t$ /1,
        $f'(t)$ /1,
        $f$/2
      }
      {$-\infty$, $1$, $3$, $+\infty$}%
      \tkzTabLine{,+,z,-,z,+}
      \tkzTabVar{-/, +/2, -/{$0,8$}, +/}
    \end{tikzpicture}
  \end{center}

  Les valeurs des extremums de $f$ ont été calculées avec :

  \[\left\{\begin{array}{l}
      f(1)=0,3\times1^3-1,8\times1^2+2,7\times1+0,8=2\\
      f(2)=0,3\times2^3-1,8\times2^2+2,7\times2+0,8=0,8\\
\end{array}\right.\]

      \end{enumerate}
    \item
      \begin{enumerate}
        \item \emph{Au bout de combien d'heures la glycémie du patient est-elle maximale et que vaut- elle ?}

          On voit sur le tableau de variations de $f$ que le maximum de $f$ est atteint en $t=1$, donc la glycémie est maximale au bout d'une heure, où elle atteint $f(1)=\qty{2}{g.L^{-1}}$.
        \item \emph{Peut-on suspecter un diabète chez le patient ? Expliquer.}

          L'énoncé dit qu'une glycémie \enquote{est normale lorsqu'elle est comprise entre \SI{0,7}{g.L^{-1}} et \SI{1,1}{g.L^{-1}} à jeun et lorsqu'elle est inférieure à \SI{1,4}{g.L^{-1}}, une heure et trente minutes après un repas.}

        Nous avons montré que $f0)=0,8$, donc la glycémie est normale à jeun. En revanche :
        \[f(1,5)=0,3\times1,5^3-1,8\times1,5^2+2,7\times1,5+0,8=1,8125\]
        donc au bout d'une heure et trente minutes, la glycémie est trop élevée.

        On peut donc suspecter un diabète chez le patient.
      \end{enumerate}
  \end{enumerate}
\end{exercice}

\pagebreak

\begin{exercice}[D'après l'exercice 4 du sujet d'EC \no66]
  \begin{em}
    La courbe $C$ tracée ci-dessous représente la masse, en kilogramme, d'un sportif en fonction du temps, exprimé en nombre d'années, sur une période de 5 ans.

    \begin{center}
      \begin{tikzpicture}[scale=1,very thick]
        \begin{axis}[
            xscale=1.7,
            yscale=1.2,
            domain=0:5,
            ultra thick,
            axis lines=middle,
            minor x tick num=9,
            minor y tick num=1,
          %enlarge y limits=true,
          %xtick={-8, -7, ..., 14},
            ytick={70, 72, ..., 86},
          %major grid style={black, thick},
            grid=both,
            ymin=70,
            ymax=88,
            xlabel={Années},
            ylabel={Masse du sportif (en \si{kg})},
            y label style={at={(current axis.above origin)},anchor=north west},
          ]
          \addplot[red, ultra thick, smooth]{x^3-7.5*x^2+12*x+80};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{em}

  \begin{enumerate}
    \item \emph{Déterminer, sur la période étudiée, le nombre de mois pendant lesquels le sportif pèse plus de 85 kilogrammes. On répondra avec la précision permise par le graphique.}
  \end{enumerate}

  \begin{em}
    On admet que la courbe $C$ est la représentation graphique de la fonction $f$ définie sur l'intervalle $[0 ; 5]$ par :
    \[ f (x) = x^3 - 7,5x^2 + 12x + 80\]
    On note $f'$ la fonction dérivée de la fonction $f$.
  \end{em}

  \begin{enumerate}[resume]
    \item \emph{Déterminer $f ' (x)$.}
    \item \emph{Montrer que $f ' (x) = (x - 1)(3x - 12)$.}
    \item
      \begin{enumerate}
        \item \emph{Établir le tableau de signes de $f ' (x)$ sur l'intervalle $[0 ; 5]$.}
        \item \emph{En déduire le tableau de variations de la fonction $f$ sur l'intervalle $[0 ; 5]$.}
        \item \emph{Déterminer la masse minimale et la masse maximale du sportif sur la période étudiée.}
      \end{enumerate}
  \end{enumerate}
\end{exercice}

\end{document}

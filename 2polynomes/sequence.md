*Ceci est un quasi copier-coller du cours de ma collègue Marie-Anne T. Merci à elle.*

# Intro

Activité 2 p. 108

# Taux de variations

Cours (fiche)

# Fonctions polynômes de degré 2

- Activités 2 et 4 p. 108+.
- Cours (fiche)

# Fonctions polynômes de degré 3

- Activités 6 p. 116
- Cours (fiche)

# Équations du second et troisième degré

- Cours (fiche)

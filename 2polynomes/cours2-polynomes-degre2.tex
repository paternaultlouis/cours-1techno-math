%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2021-2023 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compiler avec lualatex:
%$ lualatex $basename

\documentclass[10pt]{article}

\usepackage{2324-pablo}
\usepackage{2324-pablo-paternault}
\usepackage{2324-pablo-math}
\usepackage{2324-pablo-tikz}

\usepackage[
  a5paper,
  includehead,
  margin=7mm,
  headsep=1mm,
]{geometry}
\usepackage{2324-pablo-header}
\fancyhead[L]{\textsc{Ch. 2 --- Polynômes}}
\fancyhead[R]{\textsc{2 --- Polynômes du second degré}}

\usepackage{tabularx}
\usepackage{emoji}
\usepackage{multicol}

\renewcommand{\thesubsubsection}{(\alph{subsubsection})}

\begin{document}

\subsubsection{Définition}

\begin{definition*}
  Les fonctions de la forme $x\mapsto ax^2+b$ (avec $a$ et $b$ des nombres réels, et $a\neq0$) et $x\mapsto a\left( x-x_1 \right)\left( x-x_2 \right)$ (avec $a$, $x_1$, $x_2$ des nombres réels, et $a\neq0$) sont des \emph{polynômes du second degré}.
\end{definition*}

\begin{exemple}
  Identifier les nombres $a$, $b$, $x_1$ ou $x_2$ dans les polynômes suivants.

  \begin{tabularx}{\linewidth}{*{3}{X}}
    $f(x)=4x^2+7$ &
    $g(x)=-x^2-12$ &
    $h(x)=-7x^2+2$ \\
    \\
    $k(x)=7(x-2)(x-8)$ &
    $l(x)=-2(x-5)(x+2)$ &
    $m(x)=8(x+1)(x-7)$ \\
    \\
    $n(x)=(x-\pi)(x+\sqrt{2})$ &
    $p(x)=x^2$ &
    $q(x)=2x(x+1)$ \\
  \end{tabularx}
\end{exemple}

\begin{remarque*}
  Il existe d'autres polynômes du second degré d'une forme différente, mais elles ne sont pas étudiées dans ce chapitre.
\end{remarque*}

\subsubsection{Représentation graphique}

\begin{propriete*}[Représentation graphique]
  La représentation graphique d'une fonction polynôme du second degré est une \emph{parabole} :
  \begin{enumerate}
    \item si $a<0$, la fonction est d'abord croissante puis décroissante, et admet un maximum ;
    \item si $a>0$, la fonction est d'abord décroissante puis croissante, et admet un minimum.
  \end{enumerate}
\end{propriete*}

\begin{propriete*}[Sommet]~
  \begin{enumerate}
    \item Un polynôme de la forme $f(x)=ax^2+b$ a pour sommet $S(0;b)$.
    \item Un polynôme de la forme $f(x)=a\left( x-x_1 \right)\left( x-x_2 \right)$ a pour sommet $S(\alpha;\beta)$, avec $\alpha=\frac{x_1+x_2}{2}$ et $\beta=f(\alpha)$.
  \end{enumerate}
  La parabole est symétrique par rapport à la droite d'équation $x=\alpha$ (où $\alpha$ est l'abscisse de son sommet).
\end{propriete*}

\begin{exemple}
  On donne les polynômes du second degré définis sur $\mathbb{R}$ par $f(x)=3x^2+1$, $g(x)=-(x-1)(x+2)$, et $h(x)=-x^2$.
  \begin{enumerate}
    \item Identifier les nombres $a$, $b$, $x_1$ ou $x_2$ dans ces trois expressions.
    \item Dans un repère, placer le sommet de chacune des fonctions, puis tracer son allure.
    \item Déterminer graphiquement le nombre de solutions des équations $f(x)=0$, $g(x)=0$, et $h(x)=0$.
  \end{enumerate}
\end{exemple}

\begin{exercice*}
  Exercices 21 p. 120 ; 22, 26, 28, 29 p. 121.
\end{exercice*}

\subsubsection{Racines}

\begin{propriete*}[Racines]
  Soit un polynôme du second degré de la forme $f(x)=a(x-x_1)(x-x_2)$. L'équation $f(x)=0$ a deux solutions, qui sont appelées \emph{racines du polynôme}, et sont égales à $x_1$ et $x_2$.

  Dans le cas où $x_1=x_2$, il n'y a qu'une racine appelée \emph{racine double}.
\end{propriete*}

\pagebreak

\begin{exemple}~
  \begin{enumerate}
    \item Soit $f$ la fonction définie sur $\mathbb{R}$ par $f(x)=-3(x-4)(x+2)$.
      \begin{enumerate}
        \item Résoudre $f(x)=0$.
        \item Déterminer les coordonnées du sommet.
        \item Placer le sommet et les racines dans un repère, et tracer l'allure de la courbe.
        \item Par lecture graphique, dresser les tableaux de signes et de variations de $f$.
      \end{enumerate}
    \item Soit $g$ la fonction définie sur $\mathbb{R}$ par $g(x)=4x^2+1$.

      \begin{enumerate*}
        \item Résoudre $f(x)=0$.
        \item Dresser les tableaux de signes et de variations de $f$.
      \end{enumerate*}
  \end{enumerate}
\end{exemple}

\begin{exercice*}
  62, 63, 65, 66 p. 125
\end{exercice*}

\subsubsection{Variations}

\begin{propriete*}[Tableau de variations]~
  \begin{itemize}
    \item Si $f$ est de la forme : $f(x)=ax^2+b$ :
      \begin{multicols}{2}
        \begin{description}
          \item[Si $a<0$ :]
            \begin{tikzpicture}[baseline=(N11.south)]
              \tkzTabInit[lgt=.6,espcl=1.4]
              {$x$ /.5,
                $f$ /1
              }
              {$-\infty$, $0$, $+\infty$}%
              \tkzTabVar{-/, +/$b$, -/}
            \end{tikzpicture}
          \item[Si $a>0$ :]
            \begin{tikzpicture}[baseline=(N11.south)]
              \tkzTabInit[lgt=.6,espcl=1.4]
              {$x$ /.5,
                $f$ /1
              }
              {$-\infty$, $0$, $+\infty$}%
              \tkzTabVar{+/, -/$b$, +/}
            \end{tikzpicture}
        \end{description}
      \end{multicols}
    \item Si $f$ est de la forme : $f(x)=a(x-x_1)(x-x_2)$ :
      \begin{multicols}{2}
        \begin{description}
          \item[Si $a<0$ :]
            \begin{tikzpicture}[baseline=(N11.south)]
              \tkzTabInit[lgt=.6,espcl=1.4]
              {$x$ /.5,
                $f$ /1.3
              }
              {$-\infty$, $\alpha$, $+\infty$}%
              \tkzTabVar{-/, +/$f(\alpha)$, -/}
            \end{tikzpicture}
          \item[Si $a>0$ :]
            \begin{tikzpicture}[baseline=(N11.south)]
              \tkzTabInit[lgt=.6,espcl=1.4]
              {$x$ /.5,
                $f$ /1.3
              }
              {$-\infty$, $\alpha$, $+\infty$}%
              \tkzTabVar{+/, -/$f(\alpha)$, +/}
            \end{tikzpicture}
        \end{description}
      \end{multicols}
  \end{itemize}
\end{propriete*}

\begin{exemple}
  On considère les deux fonctions définies sur $\mathbb{R}$ par :
  $f(x)=-4x^2-1$ et $g(x)=2(x-4)(x+1)$. Pour chacune des deux fonctions :
  \begin{enumerate}
    \item Dresser son tableau de variations.
    \item Déterminer la valeur de ses extremums.
  \end{enumerate}
\end{exemple}

\begin{exercice*}
  87, 88 p. 125
\end{exercice*}

\subsubsection{Factorisation}

\begin{propriete*}
  Soit un polynôme du second degré de la forme $ax^2+bx+c$, ayant deux racines. Sa forme factorisée est $a(x-x_1)(x-x_2)$, où $x_1$ et $x_2$ sont ses racines.
\end{propriete*}

\begin{exemple}[\emoji{heart}]
  On considère le polynôme $f$ défini sur $\mathbb{R}$ par $f(x)=3x^2+3x-60$. On cherche la forme factorisée de $f$.
  \begin{enumerate}
    \item Vérifier que $4$ est bien une racine de $f$.
    \item Dans l'expression $a(x-x_1)(x-x_2)$, remplacer $a$ et $x_1$ par leur valeur.
    \item Exprimer $f(0)$ de deux manières différentes.
    \item En déduire la valeur de $x_2$, et conclure en donnant la forme factorisée de $f$.
  \end{enumerate}
\end{exemple}

\begin{exercice*}[Problèmes]
  70, 78 p. 124.
\end{exercice*}

\end{document}

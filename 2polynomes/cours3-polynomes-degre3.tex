%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2021-2022 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compiler avec lualatex:
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2122-pablo}
\usepackage{2122-pablo-paternault}
\usepackage{2122-pablo-math}
\usepackage{2122-pablo-tikz}

\usepackage[
  headsep=3mm,
  a5paper,
  includehead,
  margin=5mm,
]{geometry}
\usepackage{2122-pablo-header}
\fancyhead[L]{\textsc{Ch. 2 --- Polynômes}}
\fancyhead[R]{\textsc{3 --- Polynômes du troisième degré}}

\usepackage{tabularray}
\usepackage{multicol}

\renewcommand{\thesubsubsection}{(\alph{subsubsection})}

\begin{document}

\subsubsection{Définition}

\begin{definition*}
  Les fonctions de la forme $x\mapsto ax^3+b$ (avec $a$ et $b$ des nombres réels, et $a\neq0$) et $x\mapsto a\left( x-x_1 \right)\left( x-x_2 \right)\left(x-x_3\right)$ (avec $x_1$, $x_2$, $x_3$ des nombres réels distincts, et $a\neq0$) sont des \emph{polynômes du troisième degré}.
\end{definition*}

\begin{remarque*}
  Il existe d'autres polynômes du troisième degré d'une forme différente, mais elles ne sont pas étudiées dans ce chapitre.
\end{remarque*}

\subsubsection{Variations}

\begin{propriete*}[Variations]~
  \begin{itemize}
    \item Fonction de la forme $f:x\mapsto ax^3+b$ :
      \begin{multicols}{2}
        \begin{description}
          \item[Si $a<0$ :]
            \begin{tikzpicture}[baseline=(N11.south)]
              \tkzTabInit[lgt=.6,espcl=2]
              {$x$ /.5,
                $f$ /1
              }
              {$-\infty$, $+\infty$}%
              \tkzTabVar{+/, -/}
            \end{tikzpicture}
          \item[Si $a>0$ :]
            \begin{tikzpicture}[baseline=(N11.south)]
              \tkzTabInit[lgt=.6,espcl=2]
              {$x$ /.5,
                $f$ /1
              }
              {$-\infty$, $+\infty$}%
              \tkzTabVar{-/, +/}
            \end{tikzpicture}
        \end{description}
      \end{multicols}
    \item Fonction de la forme $f:x\mapsto a(x-x_1)(x-x_2)(x-x_3)$ :
      \begin{multicols}{2}
        \begin{description}
          \item[Si $a<0$ :]
            \begin{tikzpicture}[baseline=(N11.south)]
              \tkzTabInit[lgt=.6,espcl=0.9]
              {$x$ /.5,
                $f$ /1.5
              }
              {$-\infty$, , , $+\infty$}%
              \tkzTabVar{+/, -/, +/, -/}
            \end{tikzpicture}
          \item[Si $a>0$ :]
            \begin{tikzpicture}[baseline=(N11.south)]
              \tkzTabInit[lgt=.6,espcl=0.9]
              {$x$ /.5,
                $f$ /1.5
              }
              {$-\infty$, , , $+\infty$}%
              \tkzTabVar{-/, +/, -/, +/}
            \end{tikzpicture}
        \end{description}
      \end{multicols}
  \end{itemize}
\end{propriete*}

\begin{exemple}
  Dresser le tableau de variations des fonctions suivantes :

  \begin{enumerate*}[(a)]
    \item $f(x)=2x^3+4$
    \item $g(x)=-4(x-2)(x+1)(x+3)$
    \item $h(x)=-x^3$
  \end{enumerate*}
\end{exemple}

\begin{exercice*}
  33 p. 121
\end{exercice*}

\pagebreak

\subsubsection{Représentation graphique}

\begin{propriete*}[Allure des courbes]~
  \begin{center}
    \begin{tblr}{*{2}{X[c,m]}}
      $x\mapsto ax^3+b$ & $x\mapsto a(x-x_1)(x-x_2)(x-x_3)$ \\
      \begin{tikzpicture}[scale=1,very thick]
        \begin{axis}[
                %yscale=1.6,
            xscale=.9,
            domain=-3:3,
            ultra thick,
            axis lines=middle,
                    %minor x tick num=9,
                    %minor y tick num=4,
            enlarge y limits=true,
                    %xtick={0, 10, ..., 50},
                    %ytick={-500, -400, ..., 700},
                    %major grid style={black, thick},
            grid=both,
          ]
          \addplot[
                    %restrict y to domain=-400:700,
            red, ultra thick,
            smooth,
          ]{0.4*x*x*x+2};
          \node (t) at (axis cs:-1.5, 7.5) {$b$};
          \node (b) at (axis cs:0, 2) {$\bullet$};
          \draw[-latex, opacity=.5] (t) -- (b);
        \end{axis}
      \end{tikzpicture}
      &
      \begin{tikzpicture}[scale=1,very thick]
        \begin{axis}[
                %yscale=1.6,
            xscale=.9,
            domain=-2:3,
            ultra thick,
            axis lines=middle,
                    %minor x tick num=9,
                    %minor y tick num=4,
            enlarge y limits=true,
                    %xtick={0, 10, ..., 50},
                    %ytick={-500, -400, ..., 700},
                    %major grid style={black, thick},
            grid=both,
          ]
          \addplot[
            red, ultra thick,
            smooth,
          ]{.5*(x-1)*(x+1)*(x-2)};
          \node (texte) at (axis cs:1.5, -5) {$x_1$, $x_2$, $x_3$};
          \node (x1) at (axis cs:-1, 0) {$\bullet$};
          \node (x2) at (axis cs:1, 0) {$\bullet$};
          \node (x3) at (axis cs:2, 0) {$\bullet$};
          \draw[-latex, opacity=.5] (texte) -- (x1);
          \draw[-latex, opacity=.5] (texte) -- (x2);
          \draw[-latex, opacity=.5] (texte) -- (x3);
        \end{axis}
      \end{tikzpicture}
    \end{tblr}
  \end{center}
\end{propriete*}

\begin{exemple}~
  \begin{enumerate}
    \item On définit la fonction $f$ sur $\mathbb{R}$ par $f(x)=-2x^3+3$.
      \begin{enumerate}
        \item Identifier $a$ et $b$.
        \item Dresser le tableau de variations de $f$
	\item Résoudre $f(x)=0$ (en arrondissant les éventuelles solutions au dixième), calculer $f(0)$, puis tracer l'allure de la courbe de $f$.
        \item Dresser le tableau de signes de $f$.
      \end{enumerate}
    \item On définit la fonction $g$ sur $\mathbb{R}$ par $g(x)=2(x-2)(x+3)(x-1)$.
      \begin{enumerate}
        \item Identifier $a$, $x_1$, $x_2$, $x_3$.
        \item Dresser le tableau de variations de $g$.
        \item Quelles sont les solutions de $g(x)=0$ ?
        \item Calculer $g(0)$, puis tracer l'allure de la courbe de $g$.
        \item Dresser le tableau de signes de $g$.
      \end{enumerate}
  \end{enumerate}
\end{exemple}

\begin{propriete*}
  L'équation $a(x-x_1)(x-x_2)(x-x_3)=0$ a trois solutions : $x=x_1$, $x=x_2$ ou $x=x_3$.
\end{propriete*}

\begin{exercice*}
  110 à 114 p. 127
\end{exercice*}

\end{document}

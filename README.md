Ce dépôt contient mes supports de cours, devoirs, etc. pour une classe de première technologique (dans l'enseignement public français). Il ne contient que les sources, en LaTeX. Une version compilée et commentée de ces sources se trouve [sur mon site web](https://paternaultlouis.forge.apps.education.fr/cours-1techno-math/).

## Compilation

Pour compiler chacun de ces fichiers `tex`, vous aurez besoin des [classes `pablo*.sty`](https://forge.apps.education.fr/paternaultlouis/pablo).

Pour compiler l'ensemble de ces documents, vous pouvez utiliser le logiciel [evariste](http://framagit.org/spalax/evariste) sur le [fichier de configuration](https://forge.apps.education.fr/paternaultlouis/cours-1techno-math/blob/main/evariste.setup) présent à la source du dépôt.

## Licence

Ces documents sont publiés sous license [Creative Commons by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), qui en permet, entre autre, la modification et la réutilisation en classe, gratuitement ([j'explique ici](http://ababsurdo.fr/blog/pourquoi_publier_sous_licence_libre/) mon choix de diffusion libre).

## Contact

Pour me contacter, voir [la page dédiée](http://ababsurdo.fr/apropos/).

def maximum(a, b):
    ...

def minimum(a, b):
    ...

print(maximum(2, 7))
print(minimum(2, 7))
print(maximum(10, -1))
print(minimum(10, -1))

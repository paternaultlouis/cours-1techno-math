%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2021-2022 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compiler avec lualatex:
%$ lualatex $basename
%$ lualatex $basename

\documentclass[10pt]{article}

\usepackage[
  a5paper,
  includehead,
  margin=8mm,
  headsep=3mm,
]{geometry}

\usepackage{2122-pablo}
\usepackage{2122-pablo-paternault}
\usepackage{2122-pablo-math}
\usepackage[python]{2122-pablo-listings}

\usepackage{2122-pablo-header}
\fancyhead[L]{\textsc{Ch. 0 --- Algorithmique}}
\fancyhead[R]{\textsc{Cours}}

\usepackage{tabularx}
\usepackage{multicol}

\begin{document}

\section{Fonctions}

\begin{definition*}
  En informatique comme en mathématique, une \emph{fonction} prend des valeurs en entrée, et renvoit (ou retourne) une nouvelle valeur.
\end{definition*}

\begin{exemple}
  On considère la fonction suivante.
  \begin{lstlisting}
  def mafonction(a, b):
      return (a+b)/2
  \end{lstlisting}
  \begin{enumerate}
    \item Que renvoient les appels \lstinline{mafonction(14, 16)} et \lstinline{mafonction(12, 20)} ?
    \item À quoi sert cette fonction ?
    \item Modifier cette fonction pour qu'elle calcule et renvoit la moyenne de trois nombres. Quelle instruction utiliser alors pour calculer la moyenne des trois nombres : 7, 19, 2 ?
  \end{enumerate}
\end{exemple}

\section{Affectation}

\begin{definition*}
  Une \emph{variable} est un espace en mémoire, qui porte un nom, et à laquelle on peut affecter une valeur.

  En Python, l'affectation se note \lstinline{NOM = VALEUR}.
\end{definition*}

\begin{multicols}{2}
  \begin{exemple}~
    \begin{enumerate}
      \item Que renvoit \lstinline{tagada(0, 2)} ?
      \item Que renvoit \lstinline{tagada(3, 5)} ?
    \end{enumerate}
    ~

    \columnbreak

    \begin{lstlisting}
    def tagada(a, b):
      a = a + b
      a = 2 * a
      b = b - a
      return b
    \end{lstlisting}
  \end{exemple}
\end{multicols}

\begin{multicols}{2}
  \begin{exemple}~
    \begin{enumerate}
      \item
        Que renvoit \lstinline{mystere(10)} ?
        Que renvoit \lstinline{mystere(2)} ?

        Que remarquez-vous ?
      \item Prouver votre conjecture.
    \end{enumerate}
    ~

    \columnbreak

    \begin{lstlisting}
    def mystere(n):
      n = n + 3
      n = 6 * n
      n = n - 18
      n = n / 6
      return n
    \end{lstlisting}
  \end{exemple}
\end{multicols}

\begin{exemple}~
  On souhaite écrire un algorithme qui, étant donné deux variables $x$ et $y$, inverse leur contenu (c'est-à-dire qu'à la fin de l'exécution, la valeur de départ de $x$ se trouve dans $y$, et la valeur de départ de $y$ se trouve dans $x$).

  \begin{multicols}{2}
    \begin{enumerate}
      \item Exécuter l'algorithme avec différentes valeurs de $x$ et $y$. Fonctionne-t-il ?
      \item Corriger cet algorithme.
    \end{enumerate}
    ~

    \columnbreak

    \begin{lstlisting}
    x = y
    y = x
    \end{lstlisting}
  \end{multicols}
\end{exemple}

\begin{multicols}{2}
  \begin{exemple}~
    \begin{enumerate}
      \item Exécuter l'algorithme avec différentes valeurs de $x$ et $y$.
      \item À quoi sert-il ?
    \end{enumerate}
    ~

    \columnbreak

    \begin{lstlisting}
    x = x - y
    y = x + y
    x = y - x
    \end{lstlisting}
  \end{exemple}
\end{multicols}

\section{Conditionnelles}

\begin{definition*}
  Une \emph{instruction conditionnelle} permet d'exécuter une partie du code uniquement si une \emph{condition} est vérifiée.
\end{definition*}

\begin{exemple}~
  \begin{enumerate}
    \item Recopier et compléter le programme suivant pour que les fonctions \lstinline{maximum} et \lstinline{minimum} calculent et renvoient le plus grand (ou le plus petit) des nombres donnés en argument. Par exemple :
  ~

  \begin{multicols}{2}
    \begin{center}
      \begin{tabular}{cc}
        \toprule
        Appel & Résultat \\
        \midrule
        \lstinline+maximum(2, 7)+ & 7 \\
        \lstinline+minimum(2, 7)+ & 2 \\
        \lstinline+maximum(10, -1)+ & 10 \\
        \lstinline+minimum(10, -1)+ & -1 \\
        \bottomrule
      \end{tabular}
    \end{center}

    \columnbreak

    \begin{lstlisting}
    def maximum(a, b):
        ...

    def minimum(a, b):
        ...
    \end{lstlisting}
  \end{multicols}
  \item Quelle instruction utiliser pour calculer le maximum des deux nombres \lstinline{x} et \lstinline{y} ?
  \end{enumerate}
\end{exemple}

\begin{exemple}
  Un salon de coiffure pratique les réductions suivantes :

  \begin{multicols}{2}
    \begin{itemize}
      \item adulte : pas de réduction
      \item adolescent : 20\% de réduction
      \item enfant : 50\% de réduction
    \end{itemize}

    Compléter la fonction suivante pour qu'étant donné le prix et le statut du client, elle renvoit le prix après réduction.
    ~

    \columnbreak

    \begin{lstlisting}
    def réduction(prix, statut):
        if statut == "enfant":
            prix = ...
        if statut == "adolescent":
            prix = ...
        return prix
    \end{lstlisting}
  \end{multicols}
\end{exemple}

\begin{exercice*}
  Activité 2 page 52.
\end{exercice*}

\section{Boucles bornées}

\begin{propriete*}
  En Python, \lstinline{range(n)} permet d'énumérer \lstinline{n} nombres, de \lstinline{0} à \lstinline{n-1}.
\end{propriete*}

\begin{exemple}~
  \begin{itemize}[$\bullet$]
    \item \lstinline{range(3)} : \dotfill
    \item \lstinline{range(10)} : \dotfill
  \end{itemize}
\end{exemple}

\begin{definition*}
  Une \emph{boucle bornée} permet de répéter des instructions un nombre de fois connu à l'avance.
\end{definition*}

\begin{exemple}
  \begin{multicols}{2}
  La fonction ci-contre calcule la somme des nombres entiers de 1 à \lstinline{n}.
  \begin{enumerate}
    \item Pourquoi utiliser \lstinline{range(n+1)} plutôt que \lstinline{range(n)} ?
    \item Que renvoit \lstinline{somme(10)} ?
    \item Modifier la fonction pour qu'elle calcule et renvoit la somme des carrés des entiers de 1 à \lstinline{n}.
  \end{enumerate}
    \columnbreak

    \begin{lstlisting}
        def somme(n):
            somme = 0
            for i in range(n+1):
                somme = somme + i
            return somme
    \end{lstlisting}
  \end{multicols}
\end{exemple}

\begin{exemple}
  \begin{multicols}{2}
    Dans un parc naturel, une population de marmottes augmente de 3\% chaque année. Au début de l'étude, il y a environ 238 marmottes dans le parc.
  \begin{enumerate}
    \item Compléter : Augmenter une valeur de 3\,\% revient à la multiplier par \blanc{1,03}.
    \item Compléter la fonction suivante pour que l'appel \lstinline{marmottes(10)} calcule et renvoit le nombre de marmottes dix ans après le début de l'étude.
  \end{enumerate}
    \columnbreak

    \begin{lstlisting}
        def marmottes(n):
            population = ...
            for i in ...
                ...
            return ...
    \end{lstlisting}
  \end{multicols}
\end{exemple}

\begin{exemple}
  On simule à l'aide d'un programme informatique le lancer de pièces de monnaie, en comptant le nombre de \enquote{pile} obtenues.

    Pour cela :
    \begin{itemize}
      \item on utilise la fonction \lstinline{random()}, qui renvoit aléatoirement un nombre entre 0 et 1 ;
      \item on introduit une variable \lstinline{compteur}, qui compte le nombre de \enquote{pile} obtenues jusqu'à présent.
    \end{itemize}

    \pagebreak

  \begin{multicols}{2}
    \begin{enumerate}
      \item Compléter la ligne \ref{algo:random} pour que la condition soit vraie une fois sur deux.
      \item Compléter les autres lignes pour que la fonction compte le nombre de \enquote{pile} obtenues sur \lstinline{n} lancers.
    \end{enumerate}

    \columnbreak

    \begin{lstlisting}[mathescape=true]
  from random import random

  def piles(n):
      compteur = ...
      for i in range(n):
          if random() > ...: $\label{algo:random}$
              compteur = ...
      return compteur
  \end{lstlisting}
\end{multicols}
\end{exemple}


\section{Boucles non bornées}

\begin{definition*}
  Une \emph{boucle non bornée} permet de répéter une instruction tant qu'une condition est vraie.
\end{definition*}

\begin{exemple}
  \begin{multicols}{2}
  On cherche le plus petit nombre entier dont le carré est supérieur à 1000.
  \begin{enumerate}
    \item Compléter la fonction ci-contre pour qu'elle renvoit ce nombre.
    \item Quel est ce nombre ?
  \end{enumerate}
    \columnbreak
    \begin{lstlisting}
    def carré():
        while ...:
            ...
        return ...
    \end{lstlisting}
  \end{multicols}
\end{exemple}

\begin{exemple}
  \begin{multicols}{2}
  Une population de bactéries est composée de 7 individus. Chaque heure, la population double.
  \begin{enumerate}
    \item Quelle sera la population au bout d'une heure ? De deux heures ?
  \end{enumerate}
  On considère la fonction ci-contre, qui modélise l'évolution de la population de bactéries.

  \columnbreak

  \begin{lstlisting}
  def bactéries():
      population = 7
      heure = 0
      while population < 1000:
          heure = heure + 1
          population = ...
      return heure
  \end{lstlisting}
  \end{multicols}
  \begin{enumerate}[resume]
      \setcounter{enumi}{1}
    \item Exécuter l'algorthme, en notant les valeurs intermédiaires dans le tableau ci-desous. Quelle est la valeur renvoyée par la fonction ?
      \begin{center}
        \begin{tabularx}{\linewidth}{r|*{11}{|X}}
          Itération &&1&2&3&4&5&6&7&8&9&10\\
          \hline
          Heure &0&&&&&&&&&\\
          \hline
          Population &7&&&&&&&&&\\
        \end{tabularx}
      \end{center}
    \item Au bout de combien d'heures la population sera-t-elle supérieure à \numprint{1000} bactéries ?
  \end{enumerate}
\end{exemple}

\end{document}
